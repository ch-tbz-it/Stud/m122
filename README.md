![TBZ Logo](./x_gitressourcen/tbz_logo.png)
![m319 Picto](./x_gitressourcen/m122_picto.png)

[TOC]

# M122 - Abläufe mit einer Scriptsprache automatisieren

### Modulidentifikation
[Link](https://gitlab.com/modulentwicklungzh/cluster-platform/m122)

---

## Einführung

In diesem Modul geht es um die Automatisierung von Systemabläufen mittels einer Skriptsprache, die von einem Interpreter, dem sogenannten Scripting-"Host", ausgeführt wird. Es gibt verschiedene [Skriptsprachen](https://de.wikipedia.org/wiki/Skriptsprache) in vier Hauptanwendungsbereichen::

![](./x_gitressourcen/Anwendungsbereiche.png)

| Anwendungsbereich |  Skriptsprache (Interpreter = Scripting-"Host") z.Bsp.| Zweck der Automation (bzw. Fokus der Scriptsprache) |
| --- |:--- | --- |
| **Betriebssystem** |  **Bash (Unix, Linux, macOS)** <br> Batch (DOS, WINDOWS CMD) <br> Powershell (WINDOWS, Linux) <br> AppleScript (macOS) <br> Python| Skripte für Login, Installation, Konfiguration usw. für das Betriebssystem oder dessen Nutzer(-accounts) |
| **Applikation** | VBA (MS Office, Libre Office) <br> LUA (Für eigene App)<br> AppleScript (mac-Apps) <br> UnityScript (Unity) | Ausführen von Abläufen innerhalb der Applikation |
| **WEB-Client** | JavaScript, TypeScript, JScript, Dart, ... (Interpreter im Browser) <br> JavaApplet (Java VM) | Die Skripte machen die Webseite dynamisch |
| **WEB-Server** |  PHP, NodeJS, Perl (Interpreter im Server) <br> JavaServlet (Java VM) <br> Python| Über die Skripte werden die Anfragen abgearbeitet. Die Logik der Web-Anwendungen wird damit aufgerufen oder ausgeführt. |

### Kommandozeileninterpreter

Einige Skriptsprachen sind von den Kommandozeileninterpretern der Betriebssysteme abgeleitet. Die Interpreter sind hauptsächlich für die interaktive Benutzung, d.h. die Eingabe von Befehlen, ausgelegt. 

![](./x_gitressourcen/CompilerInterpreter.png)

*Wissenswert: Bei einem Interpreter wird der Programmtext Zeile für Zeile abgearbeitet, wobei er das Programm auf Fehler überprüft. Jeder gefundene Fehler führt bei einem Interpreter zum Abbruch der Übersetzung; die fehlerhafte Zeile wird angezeigt und kann korrigiert werden. Anschließend kann der Übersetzungsprozess neu gestartet werden. Leider wird die Fehlerprüfung auch dann beibehalten, wenn das Programm fehlerfrei und lauffähig ist, und somit recht viel Zeit erforderlich ist! Die von einem Interpreter übersetzten Programme sind relativ langsam und daher für komplexe Lösungen ungeeignet >> Hochsprachen mit Compiler.*

Die Eingabesprache wird um Variablen, arithmetische Ausdrücke, Kontrollstrukturen (if, while) und anderes erweitert und ermöglicht so die Automatisierung von Aufgaben (z.B. bei der unbeaufsichtigten Installation), indem "kleine Programme" in Dateien geschrieben werden. Diese Dateien können dann vom Interpreter ausgeführt werden. Die Dateien werden unter dem Betriebssystem Unix/Linux als **Shellskripte** bezeichnet (ausgeführt von einer der Unix-Shells bash, csh ...) oder unter DOS und Windows auch als Stapelverarbeitungsdateien oder Batch-Skripte (ausgeführt von cmd.exe, powershell.exe). (Quelle: Wikipedia)

In diesem Modul beginnen wir mit **Bash auf einem Linux-System** ...

---

## Möglicher Lektionenplan (Siehe Klassenordner)
| Tag  |  Thema | Bemerkung: Auftrag, Übungen |
|:----:|:------ |:-------------- |
|  1   |  [Linux Einführung](./01_Linux_Einf/README.md)  | Virtualisierung (oder Raspberry Pi) & Linux Distribution installieren <br> Erste Schritte mit Bash |
|  2   | [Bash Grundlagen](./02_Bash_Grundl/README.md) <br> [Bash Übungen](./03_Bash_Ueb/README.md) | Bash Grundlagen <br> Checkpoints mit Übungen --> Lösungen |
|  3   | [Bash Grundlagen](./02_Bash_Grundl/README.md) <br> [Bash Übungen](./03_Bash_Ueb/README.md) | Bash Grundlagen <br> Checkpoints mit Übungen --> Lösungen |
|  4   | [Bash Aufgaben](./04_Bash_Aufg/README.md) | Aufgaben 1 & 2  |
|  5   | [Bash Aufgaben >> Lösungen](./04_Bash_Aufg/README.md) <br> [Vorbereitung LB1 & Lösungen](./05_Bash_Vorb_LB1/README.md) <br> - <br> **LB1 (1-2 Lektionen)** |  Repetition, Aufgaben fertig --> Lösungen                     |
|  6   | **LB2 Start: Projekt** <br> P-Antrag <br> P-Design | [Projektauftrag](https://gitlab.com/ch-tbz-it/Stud/m122/-/tree/main/10_Projekte_LB2?ref_type=heads) <br> **Meilenstein A** Abgabe Projektantrag |
|  7   | P-Code | **Meilenstein B** Abgabe Design (Aktivitätsdiagramm aka m319) |
|  8   | P-Code  |                       |
|  9   | *(P-Code)* |     *Reserve*                   |
|  9/10   | **Projektabgabe** <br> Demo, P-Doku, P-Test | **Meilenstein C** Projektdemo & Abgabe Code mit Kommentar (TBZ Code Conventions) <br> **Meilenstein D** Abgabe: Testbericht und Doku                      |


## Projekt Rahmenbedingungen

* Kann in einer beliebigen Scriptsprache umgesetzt werden, vorzugsweise in Bash! (Alternativen falls bekannt: Windows Powershell, Python für APIs)
* Themen: Benutzerverwaltung, Netzwerk, Datenmanagement, Transfer oder Analyse von Informationen, ...
* Das Script sollten automatisch gestartet werden (CronJob).
* Einstellungen via Config-Dateien. (Kein GUI!)
* Jeglicher Daten-Impport und -Export müssen robust sein, d.h. Fehler sind mit Fehlermeldungen quittiert.


<br>

---

<br>
 

## Einsatz einer KI als Tutor

Grundsätzlich ist nichts gegen den Einsatz einer KI als Tutor (Coach) einzuwenden. Hier die Vorteile, die eine KI im Bildungskontext leisten kann: 

> *DeepSeek Jan-25 / Prompt: "In welcher Phase und wie würdest du als Lernender eine KI einsetzen?"* <br> 
> KI ist ein flexibles und mächtiges Werkzeug, das in allen Phasen des Selbstlernzyklus eingesetzt werden kann. Als Lernender würde ich KI nutzen, um meinen Lernprozess zu **personalisieren**, zu **beschleunigen** und **reflektierter** zu gestalten. Gleichzeitig ist es wichtig, **kritisch mit den Ergebnissen** umzugehen und die KI als unterstützendes Werkzeug zu sehen, **nicht als Ersatz für eigenständiges Denken und Lernen.** <br> 

![KI-Tutor](x_gitressourcen/KI-Tutor.png)

Als Lernender sollte man den Grundsatz **"Ich will es selber wissen und können!"** haben! Die KI sollte deshalb _nur_ als Tutor eingesetzt werden und nicht als Lösungsmaschine. Geben Sie also nicht die Aufgabenstellung als Prompt ein, sondern lassen Sie sich von der KI in ihrem Denk- und Lösungsprozess unterstützen! Z.B. _Prompt: "Zeige mir alle Möglichkeiten einer fussgesteuerte Schleife zu implementieren, die mit einer Merker-Variable abgebrochen werden kann!"_
Um beim Prompten die besten Resultate zu erhalten, arbeiten Sie mit den korrekten Fachbegriffen, die sie vorher erlernen. Auch können Sie den KI-Output am besten verifizieren, wenn Sie bereits Grundwissen haben.

Ihre **Übungen bzw. Lernprodukte** sollten also **selber getippt** sein, damit ein Lernprozess stattfinden kann! KI-Ausgaben sollten **gelesen, verstanden, verifiziert und kommentiert** sein. **Quelle** nicht vergessen anzugeben, bei KI: Modell, Datum, Prompt!