import ftplib
import glob


def file_filename(path):
    for filename in glob.glob(path):
        file = open(filename, 'rb')
        return file, filename

def upload_files():
    ftp = ftplib.FTP("url", "username", "password")

    file_txt = file_filename("uploads\\zahlungssystem\\*.txt")
    filename_txt = file_filename("uploads\\zahlungssystem\\*.txt")
    
    file_xml = file_filename("uploads\\zahlungssystem\\*.xml")
    filename_xml = file_filename("uploads\\zahlungssystem\\*.xml")

    # zum Erstzen (leer machen) der Pfadangaben 
    filename_txt = filename_txt.replace("uploads\\zahlungssystem\\", "")
    filename_xml = filename_xml.replace("uploads\\zahlungssystem\\", "")

    ftp.cwd("in/" + "foldername")

    ftp.storbinary("STOR " + filename_txt)
    ftp.storbinary("STOR " + file_txt)
    file_txt.close()
    
    ftp.storbinary("STOR " + filename_xml)
    ftp.storbinary("STOR " + file_xml)
    file_xml.close()

    ftp.quit()