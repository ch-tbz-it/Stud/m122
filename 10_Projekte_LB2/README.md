![TBZ Logo](../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/m122_picto.png)

[TOC]

# M122 - Abläufe mit einer Scriptsprache automatisieren

**Projektaufgaben**


---

## Lernziel:

* Vertiefung der Grundlagen anhand des Projektes
* Praxisnahe Umsetzung von vorgegebenen oder eigenen Ideen
* Automatisierungs-Script mit definierter Konfiguration (oder Parameter oder Menü)
* Robuste Ausführung, d.h. mit Fehlerüberprüfung und entsprechender Fehler-Verarbeitung (Meldung, Abbruch, Wiederholung, ...)
* Ablage und Projektentwicklung auf GitLAB ! [GitLab Installation und Verwendung](https://gitlab.com/ch-tbz-it/Stud/m319/-/tree/main/N0-GitLAB#verwendung-sw-projekt-versionierung)

--- 


## Ernstgemeinte Tipps, die kostbare Zeit und Nerven sparen:

* Verwenden Sie eine *bereits bestehende* VM mit Linux, bzw. Server.
* Verwenden Sie ein API oder einen *bestehenden* (freien) Server-Dienst. (api-ninjas.com)
* Das *Unix des Mac* hat oft andere Features und Sicherheitsbarrieren als Linux!
* Verwnden Sie *keine* Windows-Server (ausser mit Powershell).
* Setzen Sie den Netzwerkadapter der VM auf *Bridge*-Modus. Scannen Sie die vom Router vegebene IP-Adresse der VM jeweils. (z.b. mit dem 'Advanced IP-Scanner.exe')
* Zur Erstellung des Aktivitätsdiagramm (UML) verwednen Sie die [Vorlage](./AD_Symbols.drawio).
* Wählen Sie als alternative Skriptsprache "Python" nur, wenn sie bereits mit der Sprache vertraut sind (die Lösungen sind nicht einfacher!).
* Arbeiten Sie mit *crontab* und Log-Files und überprüfen Sie immer alles.
* Probieren Sie jeden winzigen Schritt aus, bevor Sie ihn in ein Skript hinein schreiben.
* Wenn Sie Lösungsteile aus dem Internet holen (z.B. ChatGPT usw.), müssen Sie jedes Detail erklären können.


--- 

<br>



# Projektarbeiten LB2

Ziel ist es, selbständig Projektideen umzusetzen um sich mit der Skriptsprache möglichst praxisnah vertraut und "fit" zu machen.


Wichtig: 

- Es werden keine Skripts akzeptiert, die einen Benutzerdialog erfordern. In diesem Modul ist die "Automatisierung" gefragt.
Also keine Spiele wie TicTacToe, VierGewinnt oder Hangman usw.

## Kleinere Projekte

(MUH 2024)

| Projekt | Punkte | Zusatz-<br>Bonus| Alleine-<br>Bonus | Aufgabenstellung |
|----     |----    |----  |----               |----     |
| **A**.) |  6     |    1 |   | [Dateien und Verzeichnisse anlegen](./A_verzeichnisse-und-dateien-anlegen)  |
| **B**.) |  6     |    5 | 1 | [Systemleistung abfragen](./B_systemleistung-abfragen)                      |
| **C**.) |  7     |    4 | 1 | [Emailadressen und Brief erstellen](./C_emailadressen-erzeugen)             |
| **D**.) |  8     |    1 | 1 | [Aktuelles Wertschriften-Depot](./D_aktuelles-wertschriften-depot)          |
| **E**.) |  6     |    1 | 1 | [QR-Rechnungen erzeugen lassen](./E_qr-rechnungen-erzeugen)                 |
| **F**.) |  5     |    6 | 1 | [APIs-Abfragen mit Datendarstellung](./F_api-abfragen-mit-datendarstellung) |
| **G**.) |  5     |    2 | 1 | [API abfragen mit Applikation](./G_api-abfragen-mit-applikation)            |
| **H**.) | 5-8    |    + | 1 | [Automatisierte Installation](./H_automatisierte-Installation)              |  
| **XX**  |  ?     |      | 1 | Eigene Projekte möglich. <br> Lassen Sie sich inspirieren von: [Bundesamt für Statistik BFS](https://www.bfs.admin.ch/bfs/de/home.html) oder [Statistik & Daten Kanton Zürich](https://www.zh.ch/de/politik-staat/statistik-daten.html) oder andere Daten wie z.B. API-Anbindung an Homegate (Wohnungssuche-Filter) oder API-Anbindung an Verkehrsbetriebe, Tram-Haltestellen o.ä.<br><br>Punkte und Umfang sind VORHER!! mit der Lehrperson abzusprechen. |


Klassen-Bewertungs[-Vorlage-für-LP.xlsx]((Vorlage-fuer-LP)_AP21a_ProjektNoten.xlsx)

<br>

### Weitere Ideen für BASH und Linux:

**Erhebung von RAW-Daten** vom Kundensystems / -dienstes (Linux Server, Raspberry Pi, Web-API):

- Analysedaten: Daten von Analyse-Tool, WEB-APIs, Datengeneratoren, ...
- SQL-Daten: Serverstatus, Berechtigungen, Tabellendaten bestimmter Datenbanken (.sql), ...
- Strukturierte Daten: Dateisystem, Berechtigungen, .cvs, .json, ...
- Verschlüsselte Daten: Transfer der Daten und öffentlichen Schlüsseln, Entschlüsseln, ...
- Status / Performance Daten: Systemdaten, Daten von Perfomance-Tools, Dienstdaten, ...
- IoT-Daten: Sensordaten, MQTT-Kanäle, IoT-WEB-Daten, Cloud-Daten, ...

**Verarbeitung und Übermittlung von FMT-Daten** an Kundensystems / -dienstes (Linux Server, Raspberry Pi, Web-Dienst):

- Execution: Ausführen von weiteren Tools (Install, Clean, ...)
- Config: Setzen von Konfigurationen (System, Tools, Docker, ...)
- Cloud: Transfer in die Cloud (AWS, Azure, ...)
- Publish: Darstellen der Daten (WEB, Print, Statistik-Tool, Graphik-Tool, ...)
- DB Storage: Datenbank SQL insert, ...
- Communication: Teams, Slack, Messenger, WAPP Twitter, Tick Tock, ...

<br>


### Zusätzliche Rahmenbedingungen 

* **Zweier- oder Einzelarbeit**. (Bei Zweierarbeit muss jedes Teammitglied eine selbst geschriebene Programmversion abgeben und demonstrieren --> im GitLAB jeweils Branch einrichten. Design und Recherchen der Projektidee darf gemeinsam erfolgen!)
* Aus dem **fremd-kopierte oder -generierte Programmteile** sind sauber zu dokumentieren (-> Doku) und Inline zu kommentieren, am besten mit Quellenangabe!
* Dokumentation von Design und/oder Implementations-Teilen mit **UML Aktivitätsdiagramm**. (Siehe m319: [UML AD](https://gitlab.com/ch-tbz-it/Stud/m319/-/tree/main/N1-UML_Activity_Diagram), [More_AD](https://gitlab.com/ch-tbz-it/Stud/m319/-/tree/main/N3-More_AD))
* Abgabe mittels **Demo auf eigenem System** und **Code-Review**.
* **Bewertung**: Siehe von LP abgegebenes [Bewertungsraster](./LB2 Projektarbeit Bewertung V1.1.docx).

<br>


### Vorgehen Implementation (Einzelarbeit!)
- **Schrittweise Implementation der einzelnen Features**.
- Jeder Feature-Schritt gleich mit Testdaten (OK/FAIL) **testen und kurz dokumentieren**. (Es soll immer darauf geachtet werden, dass bereits implementierte Features weiterhin korrekt ausgeführt werden, wenn die neu implementierten Features fertiggestellt sind.)
- Jeder Feature-Schritt gleich mit **Inline-Kommentar dokumentieren** gemäss [TBZ-Konvention](https://gitlab.com/ch-tbz-it/Stud/m319/-/tree/main/N3-Code_Formatting#quelltext-konvention-tbz-it).
- Spätestens nach jedem getesteten Feature-Schritt eine **Commit-Push-Sequenz auf GitLAB**. (Z.B. mit Tool [GitHUB Desktop](https://gitlab.com/ch-tbz-it/Stud/m319/-/tree/main/N0-GitLAB#verwendung-von-github-desktop))

<br> 


## Grösseres Projekt

Folgendes Systemdesign sollte ihrer Projektidee zugrunde liegen: 

![Systemdesign](./x_gitressourcen/Systemdesign.png)

> Diese Vorlage kann [kopiert](https://miro.com/app/board/uXjVM8D_7t0=/?share_link_id=563348319776) und für die Anforderungsdefintion angepasst werden! [Oder laden Sie die Backup-Datei](./m122-Projekte.rtb)<br>



Muss:

- Zentrales **Automationsscript** [`/IhrSystem/automate.sh param1 param2 ...`]
- **Anfrage** auf ein Kundensystem (Server/Dienst): `get: ssh / ftp / request / ...`
- **Verarbeitung** der angeforderten Informationen gemäss Projektanforderung: `data.raw`
- **Weiterreichen** der aufbereiteten Information an weiteres Kundensystem (Server/Dienst): `data.fmt`
- Bei **undefiniertem Zustand oder Datenvorkommen** soll Verarbeitung unterbunden und entsprechend Meldung gemacht werden.

Gewünscht:

- Automation: **CronJob**
- Definierte **Konfiguration**: `ihr_System.cfg`
- Laufende **Protokollierung** der Ausführung: `ihr_System.log`
- **Meldung** an Kundensystem, dass Information abgeholt wurde: `data.zip`
- **Versenden** von Informationen an ein Admin per Mail: `info.mail`




### Meilensteine

|     Meilenstein      | Auftrag | Termin |
|:--------------------:|---|---|
|  MS A <br> Teamnote  | Auftrag von LP *oder* <br> Projekt-Idee in der Anforderungsdefinition formulieren. LP nimmt Anforderungsdefinition ab! <br> GitLAB Link z.H. LP! ("M122-Klasse-THEMA-NAME") | 1. Wo |
| Realisationsphase | Mündliche Statusberichte an LP und regelmässige GitLab Pushs |
| MS B <br> Einzelnote | **Demonstration** des eigenen Automatisierungs-Script auf der persönlichen Systemumgebung (an der TBZ). **Code-Review** des Automatisierungs-Script mit der LP | Gemäss Aufgebot |
| MS C <br> Einzelnote | Abgabe von **Quelltext** [gemäss TBZ-Konvention](https://gitlab.com/ch-tbz-it/Stud/m319/-/tree/main/N3-Code_Formatting#quelltext-konvention-tbz-it), **Projektdoku** inkl. **UML-AD** (siehe oben) auf **GitLAB** | Am Abgabetermin|

### Bewertungsraster

Die Bewertung ihres Projektes kann hier eingesehen werden: [Bewertungsraster](./LB2 Projektarbeit Bewertung V1.2.docx)


<br> 

---


# Links:

[Tools & Technics](./tools-technics)

[Free API-Webseiten](https://rapidapi.com/collection/list-of-free-apis)

[Free FTP Service](https://www.bplaced.net/)



