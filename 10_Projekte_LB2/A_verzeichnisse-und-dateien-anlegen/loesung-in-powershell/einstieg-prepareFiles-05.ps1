
$verzeichnis_namensdateien = "_namensdateien"
$allFiles = (dir $verzeichnis_namensdateien).name 

# Mir fehlt nun noch, dass ich den Dateinamen inkl. dem Punkt habe.
# aber ich brauche nur den Teil vor dem Punkt.
# Um das zu machen, gibt es eine "split"-Funktion
# Nach dem Split, brauche ich dann die erste Position (=0)
foreach ($file in $allFiles) {
    $fileNameVorPunkt = $file.split(".")[0]
    echo $fileNameVorPunkt
    echo ""
}

# Skript anhalten, um zu sehen, obs Fehler gab
pause