# Lösung Aufgaben 1 & 2



**Aufgabe 1.1:**

Der Dateiname, welcher die Benutzernamen beinhaltet, wird mit `$1`
übergeben: z.B. "benutzer.txt"

```console
    #!/bin/bash
    
    #In aufg1.sh gespeichert!	
    
    for user in $(cat $1); do
        useradd $user; 
    done
```

```console
[user@host ~]$ [sudo] chmod +x ./aufg1.sh
[user@host ~]$ [sudo] ./aufg1.sh benutzer.txt

[user@host ~]$ cat /etc/passwd
```

**Aufgabe 1.2:**

Der Dateiname, welcher die Gruppen beinhaltet, wird mit dem ersten Parameter `$1` übergeben,
der Benutzernamen mit den zweiten Parameter `$2`.

 
```console
	#!/bin/bash
	for group in $(cat $1); do
	    groupadd -f $group | usermod -a -G $group $2 #$2 ist username
	done
```

```console
[user@host ~]$ [sudo] ./aufg2.sh gruppen.txt 'benutzer'

[user@host ~]$ cat /etc/passwd
[user@host ~]$ id 'benutzer'
```

**Aufgabe 1.3:**

Der Benutzername wird mit `$1` übergeben. (Verzeichnis `user` anpassen) 

```console
    #!/bin/bash
	name=$1_$(date '+%y-%m-%d').tar.gz;
	find /home/user/* -user $1 -exec cp {} /home/user/Docs/found/ \;
	tar -zcvf /home/user/Docs/found/$name /home/user/Docs/found/;
	find /home/user/Docs/found/ -type f ! -name $name -delete;
```

**Aufgabe 1.4:**

Das Tool `fping` muss installiert sein (`[user@host ~]$ [sudo] apt-get install fping`).

```console
    #!/bin/bash
    for i in $( ifconfig | grep "inet" | grep -v "127.0.0.1" | cut -d ":" -f 2 | cut -d "." -f 1-3 ); do
        for k in $(seq 1 255); do
            fping -c 1 -t250 $i.$k 2>&1 |  grep " 0% " | cut -d " " -f 1 >ips.txt
        done
    done

    #alternative Lösung:
    fping -g -c 1 -t250 172.16.6.0/24 2>&1 | grep " 0% " | cut -d " " -f 1 ips.txt
```

**Aufgabe 2.1:**

Wenn Sie keinen Zugriff auf `/root/trash` haben, nehmen Sie einen anderen Ordner...

```console
    [root@host: ~]# [sudo] mkdir /root/trash
    [root@host: ~]# [sudo] touch /root/trash/file{1..10}
    [root@host: ~]# [sudo] nano /root/trash.sh
    
    
     #!/bin/bash
     rm /root/trash/*
    
    
    [root@host: ]# [sudo] chmod +x trash.sh
    [root@host: ]# [sudo] crontab -e
    
     */5 * * * * /root/trash.sh
    
    [root@host: ]# watch ls /root/trash  
    
    (Warten bis files verschwinden --erfolgreiche Ausführung)
 ```


**Aufgabe 2.2:**

IP wird als `$1` übergeben, *ban* oder *unban* als `$2`.

```console
    #!/bin/bash
    if [ $2 -eq "ban" ]; then
        echo "banning " $1
        iptables -A INPUT -s $1 -j DROP
    elif [ $2 -eq "unban" ];then
        echo "unbanning " $1
        iptables -D INPUT -s $1 DROP
    else
        echo "Verwendung:"
        echo "1.Arg: IP-Adresse" 
        echo "2.Arg.: ban oder unban"  
        echo "Beispiel: ./ban.sh 192.168.13.3 ban"
    fi
```

**Aufgabe 2.4:**

Setzen des SGID Bits (3 verschiedene Varianten):

```console
    [user@host: ~]$ chmod g+s  /data/myFile 
    [user@host: ~]$ chmod +s   /data/myFile
    [user@host: ~]$ chmod 2755 /data/myFile
```

Setzen des Sticky Bits (3 verschiedene Varianten):

```console
    [user@host: ~]$ chmod o+t  /data 
    [user@host: ~]$ chmod +t   /data
    [user@host: ~]$ chmod 1755 /data
```